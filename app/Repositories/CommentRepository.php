<?php

namespace App\Repositories;

use App\Exceptions\Model\ValidationException;
use App\Models\Comment;

class CommentRepository extends BaseRepository
{
    public function model()
    {
        return 'App\Models\Comment';
    }

    /**
     * @param array $attributes
     *
     * @return static
     *
     * @throws ValidationException
     */
    public function create(array $attributes)
    {
        $validation = $this->validate($attributes);

        if ($validation->passes()) {
            return $this->model->create($attributes);
        } else {
            throw new ValidationException($validation->errors()->getMessages());
        }
    }

    /**
     * @param $uid
     * @param $ip
     * @param array $attributes
     *
     * @return bool|int
     *
     * @throws ValidationException
     */
    public function update($uid, $ip, array $attributes)
    {
        $comment_model = $this->model->find($uid);

        if ($comment_model instanceof Comment) {
            if ($this->checkIpAndDate($comment_model, $ip)) {
                $validation = $this->validate($attributes, $this->model->update_rules);

                if ($validation->passes()) {
                    return $comment_model->update($attributes);
                } else {
                    throw new ValidationException($validation->errors()->getMessages());
                }
            }
        }

        return false;
    }

    /**
     * @param $uid
     * @param $ip
     *
     * @return bool|null
     */
    public function delete($uid, $ip)
    {
        $comment_model = $this->model->find($uid);

        if ($comment_model instanceof Comment) {
            if ($this->checkIpAndDate($comment_model, $ip)) {
                return $comment_model->delete();
            }
        }

        return false;
    }

    /**
     * @param $model
     * @param $ip
     *
     * @return bool
     */
    private function checkIpAndDate($model, $ip)
    {
        if ($model->ip == $ip) {
            if (time() - $model->updated_at->timestamp < 3600) {
                return true;
            }
        }

        return false;
    }
}