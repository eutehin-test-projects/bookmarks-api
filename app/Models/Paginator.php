<?php

namespace App\Models;

use Illuminate\Pagination\LengthAwarePaginator;

class Paginator extends LengthAwarePaginator
{
    const PER_PAGE = 3;

    public static function initiate($items, $total, $perPage, $currentPage = null, array $options = [])
    {
        $perPage = $perPage ?: static::PER_PAGE;

        return new self($items, $total, $perPage, $currentPage, $options);
    }

    public function toArray()
    {
        return [
            'data' => $this->items->toArray(),
            'pagination' => [
                'total'         => $this->total(),
                'per_page'      => $this->perPage(),
                'current_page'  => $this->currentPage(),
                'last_page'     => $this->lastPage(),
                'next_page_url' => $this->nextPageUrl(),
                'prev_page_url' => $this->previousPageUrl(),
                'from'          => $this->firstItem(),
                'to'            => $this->lastItem(),
            ]
        ];
    }

    public function firstItem()
    {
        if ($this->count() > 0) {
            return parent::firstItem();
        }

        return 0;
    }

    public function lastItem()
    {
        if ($this->count() > 0) {
            return parent::lastItem();
        }

        return 0;
    }

    public static function resolveCurrentPath($default = '/')
    {
        $matches = [];

        $path = parent::resolveCurrentPath($default);

        preg_match('/^https/', $path, $matches);

        if (empty($matches)) {
            return preg_replace('/^http/', 'https', $path);
        }

        return $path;
    }
}