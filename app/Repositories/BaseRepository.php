<?php

namespace App\Repositories;

use App\Exceptions\RepositoryException;
use App\Models\BaseModel;
use App\Models\Paginator;
use Illuminate\Container\Container as App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class BaseRepository
{
    const PER_PAGE = 10;

    const DEFAULT_SORT_FIELD      = 'created_at';
    const DEFAULT_SORT_DESCENDING = true;

    /**
     * @var App
     */
    protected $app;
    /**
     * @var BaseModel
     */
    protected $model;

    public abstract function model();

    /**
     * BaseRepository constructor.
     *
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;

        $this->makeModel();
    }

    /**
     * @return Model|mixed
     *
     * @throws RepositoryException
     */
    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }

    /**
     * @param array      $attributes
     * @param null|array $another_rule
     *
     * @return \Illuminate\Support\Facades\Validator
     */
    public function validate(array $attributes, $another_rule = null)
    {
        return $this->model->validate($attributes, $another_rule);
    }

    /**
     * @param Collection $items
     * @param $query_parameters
     * @param bool $use_default_sort
     *
     * @return mixed
     */
    public function paginateResult(Collection $items, $query_parameters, $use_default_sort = false)
    {
        $page     = (int)array_get($query_parameters, 'page', 1);
        $per_page = (int)array_get($query_parameters, 'per_page', self::PER_PAGE);

        if ($use_default_sort) {
            $items = $items->sortBy(static::DEFAULT_SORT_FIELD, SORT_NUMERIC, static::DEFAULT_SORT_DESCENDING);
        }

        return Paginator::initiate($items->forPage($page, $per_page), $items->count(), $per_page, $page, [
            'query' => $query_parameters,
            'path'  => Paginator::resolveCurrentPath()
        ])->toArray();
    }
}