<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use App\Repositories\BookmarkRepository;
use Illuminate\Http\Request;

class BookmarkController extends Controller
{
    protected $repository;

    public function __construct(BookmarkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @api {post} /bookmark Store new bookmark
     * @apiVersion 1.0.0
     * @apiName StoreBookmark
     * @apiGroup Bookmark
     *
     * @apiParamExample {json} Request:
     * {
     *   "url": "string"
     * }
     *
     * @apiSuccess (201) {Object[]} response Success response object
     * @apiSuccess (201) {String} response.uid Created bookmark UID
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 201 Created
     * {
     *   "uid": "string"
     * }
     *
     * @apiUse HttpRouteNotFound
     * @apiUse EntityValidationError
     *
     * @apiSampleRequest /bookmark
     */

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $new_bookmark_model = $this->repository->create($request->request->all());

        return response()->json([
            'uid' => $new_bookmark_model->getKey()
        ], 201);
    }

    /**
     * @api {get} /bookmarks/last-ten Retrieve last 10 added bookmarks
     * @apiVersion 1.0.0
     * @apiName GetALastTenBookmarks
     * @apiGroup Bookmark
     *
     * @apiParam {Number} [per_page=3] Element on page
     * @apiParam {Number} [page=3] Page number
     *
     * @apiSuccessExample Response:
     * HTTP/1.1 200 OK
     *  {
     *    "data": [
     *      {
     *        "uid": integer,
     *        "url": "string",
     *        "created_at": "string",
     *        "updates_at": "string"
     *      }
     *    ],
     *    "pagination": {
     *      "total": 12,
     *      "per_page": "10",
     *      "current_page": 1,
     *      "last_page": 2,
     *      "next_page_url": "http://bookmark-api.local/bookmark/last-ten?per_page=10&page=2",
     *      "prev_page_url": null,
     *      "from": 1,
     *      "to": 1
     *    }
     *  }
     *
     * @apiUse HttpRouteNotFound
     *
     * @apiSampleRequest /bookmarks/last-ten
     */

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function showLastTen(Request $request)
    {
        return response()->json($this->repository->getLastTen($request->query->all()));
    }

    /**
     * @api {get} /bookmark/by-url Retrieve bookmark by url
     * @apiVersion 1.0.0
     * @apiName GetBookmarkByUrl
     * @apiGroup Bookmark
     *
     * @apiParam {String} [url=http://some.url] Unique url
     *
     * @apiSuccess (200) {Number} uid Bookmark UID
     * @apiSuccess (200) {String} url Bookmark URL
     * @apiSuccess (200) {String} created_at Date created
     *
     * @apiSuccessExample Response:
     *  HTTP/1.1 200 OK
     *  {
     *    "uid": integer,
     *    "url": "string",
     *    "created_at": "string"
     *  }
     *
     * @apiUse HttpRouteNotFound
     *
     * @apiSampleRequest /bookmark/by-url
     */

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByUrl(Request $request)
    {
        $bookmark = $this->repository->getByUrl($request->query->get('url'));

        if ($bookmark instanceof Bookmark) {
            return response()->json($bookmark);
        }

        return response()->json([], 404);
    }
}