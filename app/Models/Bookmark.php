<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Bookmark extends BaseModel
{
    use SoftDeletes;

    protected $table    = 'bookmarks';
    protected $fillable = ['url'];
    protected $hidden   = ['deleted_at', 'updated_at'];

    public $create_rules = [
        'url' => 'required|string'
    ];

    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'bookmark_uid');
    }

    public function scopeOfUrl($query, $url)
    {
        return $query->where('url', '=', $url);
    }
}