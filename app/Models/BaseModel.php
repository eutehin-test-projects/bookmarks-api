<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class BaseModel extends Model
{
    public $create_rules = [];
    public $update_rules = [];

    protected $primaryKey = 'uid';

    /**
     * @param array       $attributes
     * @param null|array  $another_rules
     *
     * @return Validator
     */
    public function validate(array $attributes, $another_rules)
    {
        if ($another_rules) {
            return Validator::make($attributes, $another_rules);
        } else {
            return Validator::make($attributes, $this->create_rules);
        }

    }
}