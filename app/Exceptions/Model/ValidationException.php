<?php

namespace App\Exceptions\Model;

class ValidationException extends BaseModelException
{
    protected $http_code = 400;

    /**
     * @apiDefine EntityValidationError
     *
     * @apiError (Error 400) {Object} error Object keeps error message
     * @apiErrorExample {json} Event validation errors:
     * HTTP/1.1 400 Bad Request
     * {
     *   "error": {
     *     "model_name": {
     *       "field_name": [error_code, …],
     *       …
     *     }
     *   }
     * }
     */

    public function __construct($error_codes)
    {
        $this->error_codes = $error_codes;
    }
}