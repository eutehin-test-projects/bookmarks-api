<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->post('/bookmark', 'BookmarkController@store');
$app->get('/bookmark/last-ten', 'BookmarkController@showLastTen');
$app->get('/bookmark/by-url', 'BookmarkController@showByUrl');

$app->post('/bookmark/{uid}/comment', 'CommentController@store');
$app->put('/comment/{uid}', 'CommentController@update');
$app->delete('/comment/{uid}', 'CommentController@destroy');
