<?php

namespace App\Http\Controllers;

use App\Repositories\CommentRepository;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * @var CommentRepository
     */
    protected $repository;

    /**
     * CommentController constructor.
     *
     * @param CommentRepository $repository
     */
    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @api {post} /bookmark/:bookmark_uid/comment Store new comment
     * @apiVersion 1.0.0
     * @apiName StoreComment
     * @apiGroup Comment
     *
     * @apiParamExample {json} Request:
     * {
     *   "text": "string"
     * }
     *
     * @apiSuccess (201) {Object[]} response Success response object
     * @apiSuccess (201) {String} response.uid Created comment UID
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 201 Created
     * {
     *   "uid": "string"
     * }
     *
     * @apiUse HttpRouteNotFound
     * @apiUse EntityValidationError
     *
     * @apiSampleRequest /bookmark/:bookmark_uid/comment
     */

    /**
     * @param $bookmark_uid
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $bookmark_uid)
    {
        $new_comment_model = $this->repository->create(array_merge($request->request->all(), [
            'ip'           => $request->ip(),
            'bookmark_uid' => $bookmark_uid
        ]));

        return response()->json([
            'uid' => $new_comment_model->getKey()
        ], 201);
    }

    /**
     * @api {put} /comment/:uid Update comment
     * @apiVersion 1.0.0
     * @apiName UpdateComment
     * @apiGroup Comment
     *
     * @apiParamExample {json} Request:
     * {
     *   "text": "string"
     * }
     *
     * @apiSuccess (202) {Object} response Response
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 202 Accepted
     *
     * @apiUse EntityValidationError
     *
     * @apiSampleRequest /comment/:uid
     */

    /**
     * @param Request $request
     * @param $comment_uid
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $comment_uid)
    {
        $result = $this->repository->update($comment_uid, $request->ip(), $request->request->all());

        if ($result) {
            return response()->json([], 202);
        }

        return response()->json([], 400);
    }

    /**
     * @api {delete} /comment/:uid Delete comment
     * @apiVersion 1.0.0
     * @apiName DeleteComment
     * @apiGroup Comment
     *
     * @apiSuccess (200) {Object} response Response
     * @apiSuccessExample {json} Response:
     * HTTP/1.1 200 OK
     *
     * @apiSampleRequest /comment/:uid
     */

    /**
     * @param $uid
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($uid, Request $request)
    {
        $result = $this->repository->delete($uid, $request->ip());

        if ($result) {
            return response()->json();
        }

        return response()->json([], 400);
    }
}