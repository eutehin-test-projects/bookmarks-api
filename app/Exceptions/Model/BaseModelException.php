<?php

namespace App\Exceptions\Model;

use App\Exceptions\ApplicationException;

abstract class BaseModelException extends ApplicationException {}