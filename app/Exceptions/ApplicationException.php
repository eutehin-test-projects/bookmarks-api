<?php

namespace App\Exceptions;

class ApplicationException extends \Exception
{
    protected $http_code;
    protected $error_codes;

    public function getMessages()
    {
        return [
            'error' => [
                'code' => $this->error_codes
            ]
        ];
    }

    public function getHttpCode()
    {
        return $this->http_code;
    }
}