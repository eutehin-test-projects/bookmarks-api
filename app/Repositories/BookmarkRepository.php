<?php

namespace App\Repositories;

use App\Exceptions\Model\ValidationException;

class BookmarkRepository extends BaseRepository
{
    public function model()
    {
        return 'App\Models\Bookmark';
    }

    /**
     * @param array $attributes
     *
     * @return static
     *
     * @throws ValidationException
     */
    public function create(array $attributes)
    {
        $validation = $this->validate($attributes);

        if ($validation->passes()) {
            return $this->model->firstOrCreate($attributes);
        } else {
            throw new ValidationException($validation->errors()->getMessages());
        }
    }

    /**
     * @param $query_parameters
     *
     * @return mixed
     */
    public function getLastTen($query_parameters)
    {
        $items = $this->model->orderBy('created_at', 'DESC')->take(10)->get();

        return $this->paginateResult($items, $query_parameters);
    }

    /**
     * @param $url
     *
     * @return array
     */
    public function getByUrl($url)
    {
        $result = [];

        if (!is_null($url)) {
            $result = $this->model->ofUrl($url)->first();

            if (!is_null($result)) {
                $result->load('comments');
            }
        }

        return $result;
    }
}