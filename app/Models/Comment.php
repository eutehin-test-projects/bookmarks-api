<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends BaseModel
{
    use SoftDeletes;

    protected $table    = 'comments';
    protected $fillable = ['text', 'bookmark_uid', 'ip'];
    protected $hidden   = ['deleted_at'];

    public $create_rules = [
        'text'         => 'required|string',
        'ip'           => 'required|string',
        'bookmark_uid' => 'required|integer'
    ];

    public $update_rules = [
        'text' => 'string',
    ];

    public function bookmark()
    {
        return $this->belongsTo('App\Models\Bookmark');
    }
}