<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @apiDefine HttpRouteNotFound
     *
     * @apiError (Error 400) {Number} error Keeps error code
     *
     * @apiErrorExample {json} HTTP route not found:
     * HTTP/1.1 400 Bad Request
     */
}
